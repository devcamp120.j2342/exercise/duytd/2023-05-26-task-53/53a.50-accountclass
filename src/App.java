import models.Account;

public class App {

    public static void main(String[] args) throws Exception {

        Account account1 = new Account("1", "John");
        Account account2 = new Account("2", "Tony", 1000);
       
        account1.credit(2000);
        account2.credit(3000);

        System.out.println(account1.toString());
        System.out.println(account2.toString());

        account1.debit(1000);
        account2.debit(5000);

        System.out.println(account1.toString());
        System.out.println(account2.toString());

        account1.tranferTo(account2, 2000);
        account2.tranferTo(account1, 2000);

        System.out.println(account1.toString());
        System.out.println(account2.toString());


    }
}
